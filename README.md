# Konux Map app

This project was created by Vlad Pintea-Gartner
using create-react-app, redux, google-maps-react, redux-form, redux-thunk
To run the project, open a terminal and execute:

```npm i
npm run start```

This will open up [localhost:3000](http://localhost:3000) where the site can be tested.

For more info:

* [create-react-app](https://github.com/facebook/create-react-app)
* [google-maps-react](https://github.com/istarkov/google-map-react)
* [react-redux](https://github.com/reactjs/react-redux)
* [redux-form](https://github.com/erikras/redux-form/issues/2263)
