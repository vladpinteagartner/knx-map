const defaultOpenMarkerState = {
  openMarker: null,
  editing: false,
  savingMarker: false
};

export default function mapSidebar(state = defaultOpenMarkerState, action) {
  switch (action.type) {
    case 'OPEN_MAP_SIDEBAR':
      return { ...state, openMarker: action.marker };
    case 'RECEIVE_MARKERS':
      return { ...state, openMarker: null };
    case 'EDIT_MARKER_INFO':
      return { ...state, editing: true };
    case 'CANCEL_EDIT_MARKER_INFO':
      return { ...state, editing: false };
    case 'SAVE_MARKER_INFO_START':
      return { ...state, savingMarker: true };
    case 'CANCEL_UPDATE_MARKER':
      return { ...state, openMarker: null, editing: false };
    default:
      return state;
  }
}