const defaultMarkersState = {
  markers: []
};

export default function markers(state = defaultMarkersState, action) {
  switch (action.type) {
    case 'FETCH_MARKERS':
      return action;
    case 'RECEIVE_MARKERS':
      return { ...state, markers: action.markers };
    case 'SAVE_MARKER_INFO_START':
      console.log('open', action.openMarker);
      const newMarkers = [].concat(state.markers);
      const index = newMarkers.findIndex(item => item.id === action.openMarker.id);
      newMarkers[index] = action.openMarker;
      return { ...state, markers: newMarkers };
    case 'ERROR_RECEIVING_MARKERS':
      console.error(action.error);
      return { ...state, markers: [] };
    default:
      return state;
  }
}