import { combineReducers } from 'redux';
import markers from './markersReducer';
import mapSidebar from './mapSidebarReducer';
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
  markers,
  mapSidebar,
  form: formReducer
});

export default rootReducer;