function url() {
  return 'http://demo3531119.mockable.io/locations';
}

export function receiveMarkers(json) {
  return { type: 'RECEIVE_MARKERS', markers: json };
}

export function receiveError(error) {
  return { type: 'ERROR_RECEIVING_MARKERS', error: error };
}

export function fetchMarkers() {
  return dispatch => {
    return fetch(url(), {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    })
    .then(response => response.json())
    .then(json => {
      dispatch(receiveMarkers(json));
    },
    error => {
      dispatch(receiveError(error));
    });
  };
}