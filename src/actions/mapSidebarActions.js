export function openMapSidebar(marker) {
  return { type: 'OPEN_MAP_SIDEBAR', marker: marker };
}

export function closeMapSidebar() {
  return { type: 'CANCEL_UPDATE_MARKER' };
}

export function editMarker() {
  return { type: 'EDIT_MARKER_INFO'};
}

export function cancelMarkerEdit() {
  return { type: 'CANCEL_EDIT_MARKER_INFO' };
}

export function triggerMarkerSave(updatedMarker) {
  return { type: 'SAVE_MARKER_INFO_START', openMarker: updatedMarker };
}

export function cancelMarkerUpdate() {
  return { type: 'CANCEL_UPDATE_MARKER' };
}