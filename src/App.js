import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import './App.css';

import KnxHeader from './components/header/header';
import KnxSensorMap from './components/sensor-map/sensor-map';

const store = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <KnxHeader />
          <main>
            <article>
              <KnxSensorMap />
            </article>
          </main>
        </div>
      </Provider>
    );
  }
}

export default App;
