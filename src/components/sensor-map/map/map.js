import React, { Component } from 'react';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import './map.css';
import { connect } from 'react-redux';
import * as mapSidebarActions from '../../../actions/mapSidebarActions';
import * as markerActions from '../../../actions/markerActions';

const mapSidebarState = function (state) {
  return {
    openMarker: state.mapSidebar.openMarker,
    editingMarker: state.mapSidebar.editing,
    savingMarker: state.mapSidebar.savingMarker,
    markers: state.markers.markers
  };
};

const style = {
  width: '100%',
  height: '100%'
}

class KnxMap extends Component {

  openSideMenu(marker) {
    this.props.dispatch(mapSidebarActions.openMapSidebar(marker));
  };

  componentWillMount() {
    this.props.dispatch(markerActions.fetchMarkers());
  }

  render() {
    return (
      <section>
        <Map google={this.props.google} zoom={10} style={style}
          initialCenter={{
            lat: 46.500,
            lng: 24.500
          }} >
          {
            this.props.markers.map( marker => {
              if (marker) {
                return <Marker key={marker.id} onClick={this.openSideMenu.bind(this, marker)}
                  name={marker.title} label={marker.label} markerID={marker.id} knxHealthStatus={marker.knxHealthStatus}
                  position={{ lat: marker.lat, lng: marker.long }} />
              } else {
                return marker;
              }
            })
          }
        </Map>
        
        <span>Loading...</span>
      </section>
    );
  }
}

const wrapperKnxMap = GoogleApiWrapper({
  apiKey: ('AIzaSyCBCWFQBzAMwj7A8yo3IOZ1gi2toHHHQOw')
})(KnxMap);

export default connect(mapSidebarState)(wrapperKnxMap);
