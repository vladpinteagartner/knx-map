import React, { Component } from 'react';
import './sensor-map.css';
import { connect } from 'react-redux';

import MapSidebar from './map-sidebar/map-sidebar';
import KnxMap from './map/map';

const markersStateProps = function (state) {
  return {
    markers: state.markers.markers
  };
};

class KnxSensorMap extends Component {
  render() {
    return (
      <div className="row">
        <MapSidebar />
        <KnxMap />
      </div>
    );
  }
}

export default connect(markersStateProps)(KnxSensorMap);
