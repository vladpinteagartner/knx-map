import React, { Component } from 'react';
import './map-sidebar.css';
import { connect } from 'react-redux';
import * as mapSidebarActions from '../../../actions/mapSidebarActions';
import { Field, reduxForm } from 'redux-form';

const mapSidebarState = function (state) {
  return {
    openMarker: state.mapSidebar.openMarker,
    editingMarker: state.mapSidebar.editing
  };
};

class MapSidebar extends Component {

  closeSideMenu() {
    this.props.dispatch(mapSidebarActions.closeMapSidebar());
  };

  editLocation() {
    this.props.dispatch(mapSidebarActions.editMarker());
  };

  cancelLocationEdit() {
    this.props.dispatch(mapSidebarActions.cancelMarkerEdit());
  };

  triggerLocationEditSave(values) {
    const newMarkerValues = Object.assign({}, this.props.openMarker, values);
    this.props.dispatch(mapSidebarActions.triggerMarkerSave(newMarkerValues));
  };

  render() {

    let locationDetails;
    if (this.props.openMarker) {
      if (!this.props.editingMarker) {
        locationDetails = (
          <div>
            <p className="point-info">
              <span>Sensor name: </span>
              <span>{ this.props.openMarker.title }</span>
            </p>
            <p className="point-info">
              <span>Status: </span>
              <span>{this.props.openMarker.knxHealthStatus}</span>
            </p>
            <p className="point-info">
              <span>Latitude: </span>
              <span>{this.props.openMarker.lat}</span>
            </p>
            <p className="point-info">
              <span>Longitude: </span>
              <span>{this.props.openMarker.long}</span>
            </p>
            <button className="map-sidebar-action-edit map-sidebar-action"
              onClick={this.editLocation.bind(this)}>Edit</button>
          </div>
        );
      } else {
        locationDetails = (
          <form onSubmit={this.props.handleSubmit(this.triggerLocationEditSave.bind(this))}>
            <div className="point-info row">
              <label className="col is-narrow edit-details">Sensor name: </label>
              <Field name="title" component="input" type="text" placeholder={this.props.openMarker.title } className="col edit-details-input" />
            </div>
            <div className="point-info row">
              <label className="col is-narrow edit-details">Status: </label>
              <Field name="knxHealthStatus" component="select" className="col edit-details-input" >
                <option>Pick a health status</option>
                <option value="good">Good</option>
                <option value="warning">Warning</option>
                <option value="bad">Bad</option>
              </Field>
            </div>
            <div className="point-info row">
              <label className="col is-narrow edit-details">Latitude: </label>
              <Field name="lat" component="input" type="text" placeholder={this.props.openMarker.lat } className="col edit-details-input" />
            </div>
            <div className="point-info row">
              <label className="col is-narrow edit-details">Longitude: </label>
              <Field name="long" component="input" type="text" placeholder={this.props.openMarker.long } className="col edit-details-input" />
            </div>
            <button className="map-sidebar-action-cancel map-sidebar-action"
              disabled={this.props.pristine || this.props.submitting}
              onClick={this.cancelLocationEdit.bind(this)}>Cancel</button>
            <button className="map-sidebar-action-save map-sidebar-action"
              disabled={this.props.pristine || this.props.submitting}
              type="Submit">Save</button>
          </form>
        );
      }
    }

    return (
      <aside className={this.props.openMarker ? "col is-3 map-sidebar" : "hide map-sidebar"}>
        <section className="map-sidebar-section">
          <span className="map-sidebar-close" onClick={this.closeSideMenu.bind(this)}>X</span>
          <h2>Sensor information</h2>
          <hr />
          {locationDetails}
        </section>
      </aside>
    );
  }
}

const wrapperMapSidebar = reduxForm({
  form: 'markerUpdate'
})(MapSidebar);
export default connect(mapSidebarState)(wrapperMapSidebar);
