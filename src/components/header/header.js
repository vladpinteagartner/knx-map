import React, { Component } from 'react';
import logo from '../../assets/images/konux-logo.svg';
import './header.css';

class KnxHeader extends Component {
  render() {
    return (
      <header className="knx-header">
        <img src={logo} className="knx-logo" alt="logo" />
      </header>
    );
  }
}

export default KnxHeader;
